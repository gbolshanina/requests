package requests

import (
	"requests/internal/input"

	"github.com/jmoiron/sqlx"
)

// DBPost ...
func DBPost(request RequestsRequest, db *sqlx.DB) (id *int64, err error) {
	_, err = db.NamedQuery(`INSERT INTO public.requests (create_at, headers, content_length) 
	VALUES(:create_at, :headers, :content_length);`, &request)
	var idc int64
	err = db.Get(&idc, `SELECT MAX(id) AS LastID FROM public.requests`)
	if err != nil {
		return nil, err
	}
	id = &idc
	return
}

// DBGetAll ...
func DBGetAll(queryParam *input.QueryParam, db *sqlx.DB) (request []Requests, count int, err error) {
	request = make([]Requests, 0)
	if queryParam.Query == nil {

		sqlStatSelectCount := `SELECT count(id) FROM public.requests`
		if err := db.QueryRow(sqlStatSelectCount).Scan(&count); err != nil {
			return nil, 0, err
		}

		sqlStatSelect := `SELECT id, create_at, headers, content_length
		FROM public.requests`

		input.GetSQLStatementWithOrderLimitOffset(queryParam, &sqlStatSelect)

		err := db.Select(&request, sqlStatSelect)
		if err != nil {
			return nil, 0, err
		}

	} else {

		sqlStatSelectCount := `SELECT count(id) FROM public."user"`
		input.GetSQLStatementWithQuery(queryParam, &sqlStatSelectCount)

		if err := db.QueryRow(sqlStatSelectCount).Scan(&count); err != nil {
			return nil, 0, err
		}

		sqlStatSelect := `SELECT id, create_at, headers, content_length
				    FROM public.requests `

		input.GetFullSQLQuery(queryParam, &sqlStatSelect)

		err := db.Select(&request, sqlStatSelect)
		if err != nil {
			return nil, 0, err
		}
	}
	return request, count, nil
}

// DBDelete ...
func DBDelete(id string, db *sqlx.DB) (err error) {
	_, err = db.Exec("DELETE FROM public.requests WHERE id = $1", id)
	if err != nil {
		return err
	}
	return nil
}