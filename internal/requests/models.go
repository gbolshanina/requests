package requests

import "time"

//Requests ...
type Requests struct {
	ID            int64     `db:"id" json:"id"`
	ContentLength int64     `db:"content_length" json:"contentLength"`
	Headers       string    `db:"headers" json:"headers"`
	CreateAt      time.Time `db:"create_at" json:"createAt"`
}

type RequestsRequest struct {
	ContentLength int64     `db:"content_length" json:"contentLength"`
	Headers       string    `db:"headers" json:"headers"`
	CreateAt      time.Time `db:"create_at" json:"createAt"`
}

type SendRequest struct {
	Method string `json:"method"`
	URL    string `json:"url"`
}
