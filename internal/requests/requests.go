package requests

import (
	"requests/internal/input"
	"requests/internal/output"

	"github.com/jmoiron/sqlx"
)

// Post ...
func Post(request RequestsRequest, db *sqlx.DB) (id *int64, outerr *output.ErrorResult) {
	id, err := DBPost(request, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return id, outerr

	}
	return
}

// GetAll ...
func GetAll(queryParam *input.QueryParam, db *sqlx.DB) (requests []Requests, count int, outerr *output.ErrorResult) {
	requests, count, err := DBGetAll(queryParam, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return nil, 0, outerr
	}
	return requests, count, nil
}


// Delete ...
func Delete(id string, db *sqlx.DB) (outerr *output.ErrorResult) {
	err := DBDelete(id, db)
	if err != nil {
		outerr := new(output.ErrorResult)
		outerr.Error = err
		outerr.OutError = "Ошибка при работе с БД: " + err.Error()
		return outerr
	}
	return nil
}