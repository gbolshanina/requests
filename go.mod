module requests

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/dimfeld/httptreemux v5.0.1+incompatible
	github.com/jackc/pgx v3.2.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/pkg/errors v0.8.0 // indirect
	github.com/rs/zerolog v1.11.0
	go.opencensus.io v0.18.0
)
