package handlers

import (
	"requests/internal/db"
	"github.com/jmoiron/sqlx"
	"requests/internal/output"
	"bytes"
	"requests/internal/input"
	"strings"
	"context"
	"encoding/json"
	"net/http"
	"time"
	"fmt"
	"requests/cmd/requests/cfg"
	"requests/internal/requests"
	"requests/internal/web"
	"github.com/rs/zerolog"
	"go.opencensus.io/trace"
)

// Requests - user handler
type Requests struct {
	config *cfg.Config
	db     *sqlx.DB
}

// Post ...
func (n *Requests) Post(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Requests.Post")
	defer span.End()

	var v requests.SendRequest
	errjson := json.NewDecoder(r.Body).Decode(&v)
	if errjson != nil {
		web.Respond(ctx, log, w, output.OutError{Error: "Ошибка при чтении JSON"}, http.StatusBadRequest)
		return nil
	}
	var addRequest requests.RequestsRequest	
	var jsonStr = []byte(`{}`)
	req, errRequest := http.NewRequest(strings.ToUpper(v.Method), v.URL, bytes.NewBuffer(jsonStr))
	if errRequest != nil {
		web.Respond(ctx, log, w, output.OutError{Error: fmt.Sprintf("Ошибка %s запроса", strings.ToUpper(v.Method))}, http.StatusBadRequest)
		return nil
	}
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")
	
	client := &http.Client{}
	resp, errPost := client.Do(req)
	if errPost != nil {
		web.Respond(ctx, log, w, output.OutError{Error: fmt.Sprintf("Ошибка %s запроса", strings.ToUpper(v.Method))}, http.StatusBadRequest)
		return nil
	}
	defer resp.Body.Close()
	addRequest.ContentLength = resp.ContentLength
	for k, v := range resp.Header {
		addRequest.Headers+=fmt.Sprintf("key: %s, value: %s;", k, v)
		}

	addRequest.CreateAt = time.Now().UTC()

	id, dbErr := requests.Post(addRequest, db.DB)

	var addRequestWithID requests.Requests
	addRequestWithID.ID = *id
	addRequestWithID.ContentLength = addRequest.ContentLength
	addRequestWithID.CreateAt = addRequest.CreateAt
	addRequestWithID.Headers =addRequest.Headers
	if dbErr != nil {
		web.Respond(ctx, log, w, dbErr, http.StatusBadRequest)
		return nil
	}

	web.Respond(ctx, log, w, addRequestWithID, http.StatusOK)

	return nil
}

// GetAll ..
func (n *Requests) GetAll(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Requests.GetAll")
	defer span.End()

	query, errs := input.GetQueryParamFromRequest(r)
	if errs != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, errs)
		web.Respond(ctx, log, w, &output.OutError{Error: errs.Error()}, http.StatusOK)
		return nil
	}
	v, count, err := requests.GetAll(query, n.db)
	if err != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, err.Error)
		web.Respond(ctx, log, w, &output.OutError{Error: err.OutError}, http.StatusOK)
		return nil
	}

	web.Respond(ctx, log, w, &output.Result{Items: v, TotalCount: count}, http.StatusOK)

	return nil
}

// Delete ...
func (n *Requests) Delete(ctx context.Context, log *zerolog.Logger, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Requests.Delete")
	defer span.End()

	err := requests.Delete(params["id"], n.db)
	if err != nil {
		log.Error().Msgf("%s | %s | %s", r.RemoteAddr, r.URL, err.Error)
		web.Respond(ctx, log, w, &output.OutError{Error: err.OutError}, http.StatusOK)
		return nil
	}

	web.Respond(ctx, log, w, nil, http.StatusOK)

	return nil
}