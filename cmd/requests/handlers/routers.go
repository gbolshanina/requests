package handlers

import (
	"requests/cmd/requests/cfg"
	"requests/internal/web"

	"net/http"

	"github.com/jmoiron/sqlx"

	"github.com/rs/zerolog"
)

// API - app api handler
func API(log *zerolog.Logger, cfg *cfg.Config, db *sqlx.DB) http.Handler {
	app := web.New(log)

	n := Requests{
		config: cfg,
		db:     db,
	}

	app.Handle("POST", "/requests", n.Post)
	app.Handle("GET", "/requests", n.GetAll)
	app.Handle("DELETE", "/requests/:id", n.Delete)

	return app
}