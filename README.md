## Quick Start

#### Run 

	go run main.go

**Добавить таблицу в postgres**

```sh
CREATE DATABASE requests OWNER = postgres;
GRANT ALL PRIVILEGES ON database requests TO postgres;

CREATE TABLE public.requests (
	id serial NOT NULL,
	create_at timestamptz NOT NULL,
	headers text NOT NULL,
	content_length int8 NOT NULL,
	CONSTRAINT requests_pk PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;

```
**[1. Послать запрос](#post)**  
**[2. Получить список запросов](#getall)**  
**[3. Удалить запрос](#delete)**  

&emsp;Запросы можно протестировать с помощью файла insomnia

****
<a name="post">  </a>

**1. ПОСЛАТЬ ЗАПРОС**

&emsp;При POST запросе к http://localhost:8070/requests отправляется запрос на указаный url. Логи ошибок пишутся в консоль.
&emsp;**http://localhost:8070/requests**
```json
{
	"method":"put",
	"url":"https://stackoverflow.com/1"
}
```
&emsp;В случае удачного запроса сервис возвращает ответ в форме:
```json
{
	"id": 41,
	"contentLength": 56153,
	"headers": "key: X-Served-By, value: [cache-hhn1529-HHN];key: X-Cache-Hits, value: [0];key: Strict-Transport-Security, value: [max-age=15552000];key: Accept-Ranges, value: [bytes];key: Date, value: [Fri, 30 Nov 2018 10:00:05 GMT];key: Via, value: [1.1 varnish];key: X-Cache, value: [MISS];key: X-Timer, value: [S1543572005.406387,VS0,VE203];key: Vary, value: [Fastly-SSL];key: Content-Length, value: [56153];key: Cache-Control, value: [private];key: X-Request-Guid, value: [398ae832-42c6-423e-9350-84430ad99309];key: Content-Security-Policy, value: [upgrade-insecure-requests];key: Set-Cookie, value: [prov=350d7cc5-37c9-8e0d-2ec3-d02c6290cb4b; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly];key: Content-Type, value: [text/html; charset=utf-8];key: X-Dns-Prefetch-Control, value: [off];",
	"createAt": "2018-11-30T10:00:05.626881591Z"
}
```
&emsp;В случае не уданого запроса сервис возвращает ответ в форме:
```json
{
	"error": "Ошибка PUT запроса"
}
```

****
<a name="getall">  </a>

**2. ПОЛУЧИТЬ СПИСОК ЗАПРОСОВ**

&emsp;При GET запросе к http://localhost:8070/requests сервис возвращает список запросов.
&emsp;**http://localhost:8070/requests?limit=3&offset=0**
&emsp;В случае удачного запроса сервис возвращает ответ в форме:
```json
{
	"items": [
		{
			"id": 37,
			"contentLength": -1,
			"headers": "key: X-Dns-Prefetch-Control, value: [off];key: Set-Cookie, value: [prov=d7de4d21-79c8-19e1-59f0-097ed1cb7702; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly];key: Content-Type, value: [text/html; charset=utf-8];key: X-Request-Guid, value: [611d6a05-6b95-4720-8772-37cf8f746ad4];key: X-Cache-Hits, value: [0];key: X-Served-By, value: [cache-hhn1529-HHN];key: Accept-Ranges, value: [bytes];key: X-Cache, value: [MISS];key: X-Timer, value: [S1543571872.840982,VS0,VE98];key: Vary, value: [Accept-Encoding,Fastly-SSL];key: Date, value: [Fri, 30 Nov 2018 09:57:51 GMT];key: Via, value: [1.1 varnish];key: Cache-Control, value: [private];key: X-Frame-Options, value: [SAMEORIGIN];key: Strict-Transport-Security, value: [max-age=15552000];key: Content-Security-Policy, value: [upgrade-insecure-requests];",
			"createAt": "2018-11-30T12:57:51.958984+03:00"
		},
		{
			"id": 38,
			"contentLength": -1,
			"headers": "key: Date, value: [Fri, 30 Nov 2018 09:58:21 GMT];key: Strict-Transport-Security, value: [max-age=15552000];key: Accept-Ranges, value: [bytes];key: X-Timer, value: [S1543571901.010356,VS0,VE88];key: Vary, value: [Accept-Encoding,Fastly-SSL];key: Content-Type, value: [text/html; charset=utf-8];key: Content-Security-Policy, value: [upgrade-insecure-requests];key: X-Served-By, value: [cache-hhn1529-HHN];key: X-Cache-Hits, value: [0];key: Set-Cookie, value: [prov=f9f578d5-f4dc-07a1-d570-7744a2710205; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly];key: Cache-Control, value: [private];key: X-Request-Guid, value: [2971502f-2a45-4ec8-a2fd-070842ba4d5a];key: Via, value: [1.1 varnish];key: X-Cache, value: [MISS];key: X-Dns-Prefetch-Control, value: [off];key: X-Frame-Options, value: [SAMEORIGIN];",
			"createAt": "2018-11-30T12:58:21.115905+03:00"
		},
		{
			"id": 39,
			"contentLength": 56153,
			"headers": "key: Content-Type, value: [text/html; charset=utf-8];key: X-Served-By, value: [cache-hhn1529-HHN];key: X-Dns-Prefetch-Control, value: [off];key: Date, value: [Fri, 30 Nov 2018 09:59:46 GMT];key: X-Cache, value: [MISS];key: X-Cache-Hits, value: [0];key: Set-Cookie, value: [prov=d876c942-02b4-2866-7a52-a0d557d15f46; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly];key: Content-Length, value: [56153];key: X-Request-Guid, value: [fcdcbf7d-64c5-4d7b-87ee-59d3b0da780a];key: Strict-Transport-Security, value: [max-age=15552000];key: Content-Security-Policy, value: [upgrade-insecure-requests];key: X-Timer, value: [S1543571987.853636,VS0,VE83];key: Vary, value: [Fastly-SSL];key: Cache-Control, value: [private];key: Accept-Ranges, value: [bytes];key: Via, value: [1.1 varnish];",
			"createAt": "2018-11-30T12:59:46.955135+03:00"
		}
	],
	"totalCount": 5
}
```
&emsp;В случае не уданого запроса сервис возвращает, например, ответ в форме:
```json
{
	"error": "Ошибка: неправильные значение в запросе(limit)"
}
```

****
<a name="delete">  </a>

**3. УДАЛЕНИЕ**
&emsp;При DELETE запросе к http://localhost:8070/1 сервис удаляет запись из базы данных.
&emsp;**http://localhost:8070/1**
&emsp;В случае удачного запроса сервис возвращает ответ в форме пустого тела и статуса 200 OK.
&emsp;В случае не уданого запроса сервис возвращает, например, ответ в форме:
```json
{
	"error": "Ошибка при работе с БД: ОШИБКА: неверное значение для целого числа: \"2ф\" (SQLSTATE 22P02)"
}
```